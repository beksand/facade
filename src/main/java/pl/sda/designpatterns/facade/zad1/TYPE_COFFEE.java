package pl.sda.designpatterns.facade.zad1;

public enum TYPE_COFFEE {
    ESPRESSO, LATTE, CAPPUCCINO, AMERYKANKA;
}
