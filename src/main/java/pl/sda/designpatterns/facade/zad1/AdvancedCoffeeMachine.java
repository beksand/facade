package pl.sda.designpatterns.facade.zad1;

public class AdvancedCoffeeMachine {
    private  CoffeeBeverage coffeeBeverage;

    public void addIngredient(CoffeeIngredient coffeeIngredient){
        coffeeBeverage.getCoffeeIngredientList().add(coffeeIngredient);


    }
    public CoffeeBeverage makeCoffeeBeverage(){
        coffeeBeverage.toString();
        CoffeeBeverage tempCoffeeBeverage = coffeeBeverage;
        coffeeBeverage = new CoffeeBeverage();
        return tempCoffeeBeverage;
    }

}
