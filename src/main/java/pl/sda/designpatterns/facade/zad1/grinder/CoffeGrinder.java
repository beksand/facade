package pl.sda.designpatterns.facade.zad1.grinder;

import pl.sda.designpatterns.facade.zad1.CoffeeBean;

public class CoffeGrinder {

    private int wasteContainerCount;

    public void empty(){
        wasteContainerCount = 0;
    }

    public CoffePowderIngredient grind(CoffeeBean coffeeBean) throws CoffieGrinderException {
        if (wasteContainerCount>3){
            throw new CoffieGrinderException("Waste container is Full");

        }
        if (wasteContainerCount<3){
            wasteContainerCount++;
            System.out.println("cmietnik+");
        }
        return new CoffePowderIngredient();
    }
}
