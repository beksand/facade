package pl.sda.designpatterns.facade.zad1;

public class WaterIngredient extends CoffeeIngredient {

    private int woda;

    public WaterIngredient(int woda) {
        this.woda = woda;
    }
}
