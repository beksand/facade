package pl.sda.designpatterns.facade.zad1;

import pl.sda.designpatterns.facade.zad1.frother.MilkFrother;
import pl.sda.designpatterns.facade.zad1.grinder.CoffeGrinder;
import pl.sda.designpatterns.facade.zad1.grinder.CoffePowderIngredient;
import pl.sda.designpatterns.facade.zad1.grinder.CoffieGrinderException;

public class Coffee {
    private AdvancedCoffeeMachine advancedCoffeeMachine;
    private CoffeGrinder coffeGrinder;
    private MilkFrother milkFrother;
    public void newCoffe(String coffe, boolean sugar) throws CoffieGrinderException {
        CoffeGrinder coffeGrinder = new CoffeGrinder();
        MilkFrother milkFrother = new MilkFrother();
        if (coffe.toUpperCase().equals(TYPE_COFFEE.ESPRESSO)){
            makeEspresso();
        }
        if (coffe.toUpperCase().equals(TYPE_COFFEE.LATTE)){
            advancedCoffeeMachine.addIngredient(coffeGrinder.grind(new CoffeeBean()));
            advancedCoffeeMachine.addIngredient(new WaterIngredient(100));
            advancedCoffeeMachine.addIngredient(milkFrother.froth(new MilkIngredient()));
            if (sugar){
                advancedCoffeeMachine.addIngredient(new SugarIngredient());
            }
        }
        if (coffe.toUpperCase().equals(TYPE_COFFEE.CAPPUCCINO)){
            advancedCoffeeMachine.addIngredient(coffeGrinder.grind(new CoffeeBean()));
            advancedCoffeeMachine.addIngredient(new WaterIngredient(30));
            advancedCoffeeMachine.addIngredient(milkFrother.froth(new MilkIngredient()));
            if (sugar){
                advancedCoffeeMachine.addIngredient(new SugarIngredient());
            }
        }
        if (coffe.toUpperCase().equals(TYPE_COFFEE.AMERYKANKA)){
            makeAmerykanka(sugar);
        }
    }
    public CoffeeBeverage makeEspresso(){
        CoffePowderIngredient grinder = null;
        while (grinder==null){
            try {
                grinder = coffeGrinder.grind(new CoffeeBean());
            } catch (CoffieGrinderException e) {
                coffeGrinder.empty();
                System.out.println("Waste cleaned!!!");
            }
        }
        advancedCoffeeMachine.addIngredient(grinder);
        advancedCoffeeMachine.addIngredient(new WaterIngredient(30));
        return advancedCoffeeMachine.makeCoffeeBeverage();
    }
    public CoffeeBeverage makeAmerykanka(boolean sugar){
        CoffePowderIngredient grinder = null;
        while (grinder==null){
            try {
                grinder = coffeGrinder.grind(new CoffeeBean());
            } catch (CoffieGrinderException e) {
                coffeGrinder.empty();
                System.out.println("Waste cleaned!!!");
            }
        }
        advancedCoffeeMachine.addIngredient(new WaterIngredient(100));
        advancedCoffeeMachine.addIngredient(grinder);
        if (sugar){
            advancedCoffeeMachine.addIngredient(new SugarIngredient());
        }
        return advancedCoffeeMachine.makeCoffeeBeverage();
    }
}
