package pl.sda.designpatterns.facade.zad1.frother;

import pl.sda.designpatterns.facade.zad1.MilkIngredient;

public class MilkFrother {


    public FoamedMilkIngredient froth(MilkIngredient milkIngredient){

        if (Math.random()<0.8){
            try {
                throw new MilkFrotherException();
            } catch (MilkFrotherException e) {
                e.printStackTrace();
            }
        }

        return new FoamedMilkIngredient();
    }
}
