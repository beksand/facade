package pl.sda.designpatterns.facade.zad1.grinder;

public class CoffieGrinderException extends Exception {
    public CoffieGrinderException(String message) {
        super(message);
    }

    public CoffieGrinderException() {

    }
}
