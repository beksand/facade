package pl.sda.designpatterns.facade.zad1;

import java.util.ArrayList;
import java.util.List;

public class CoffeeBeverage {
    private List<CoffeeIngredient> coffeeIngredientList = new ArrayList<CoffeeIngredient>();

    @Override
    public String toString() {
        return "CoffeeBeverage{" +
                "coffeeIngredientList=" + coffeeIngredientList +
                '}';
    }

    public List<CoffeeIngredient> getCoffeeIngredientList() {
        return coffeeIngredientList;
    }

    public void setCoffeeIngredientList(List<CoffeeIngredient> coffeeIngredientList) {
        this.coffeeIngredientList = coffeeIngredientList;
    }

}


